#include <iostream>
#include <fstream>
#include <string>

using namespace std;

string readLine(ifstream &file)
{
	string s;
	file >> ws;
	getline(file, s);
	
	for(int i=0; i<s.length(); i++)
	{
		if(s[i] == ' ' || s[i] == '\t')
		{
			s.erase(i, 1);
		}
	}
	
	for(int i=0; i<s.length(); i++)
	{
		if(s[i] == '/')
		{
			int j = i;
			while(s[j+1] == ' ')
			{
				j++;
			}
			if(s[j+1] == '/')
			{
				s.resize(i);
				break;
			}
		}
	}
	
	return s;
}

string binGenerator(const string &str)
{
	string s;
	if(str[0] == '@')
	{
		int num = stoi(str.substr(1, str.length()));
		for(int i=0; i<16; i++)
		{
			s.insert(0, to_string(num%2));
			num = num/2;
		}
		return s;
	}
	else
	{
		s = "111";
		string comp, dest, jmp;
		int equalIndex = str.find("=");
		int jumpIndex = str.find(";");
		if(equalIndex == -1 && jumpIndex >= 0)
		{
			comp = str.substr(0, jumpIndex);
			dest = "";
			jmp = str.substr(jumpIndex+1, str.length()-1);
		}
		else if(equalIndex >= 0 && jumpIndex == -1)
		{
			dest = str.substr(0, equalIndex);
			comp = str.substr(equalIndex+1, str.length()-1);
			jmp = "";
		}
		else
		{
			throw string("Error, not a valid asm code.");
		}
		
		
		//computation generator
		if(comp == "0")
			s += "0101010";
		else if(comp == "1")
			s += "0111111";
		else if(comp == "-1")
			s += "0111010";
		else if(comp == "D")
			s += "0001100";
		else if(comp == "A")
			s += "0110000";
		else if(comp == "!D")
			s += "0001101";
		else if(comp == "!A")
			s += "0110001";
		else if(comp == "-D")
			s += "0001111";
		else if(comp == "-A")
			s += "0110011";
		else if(comp == "D+1" || comp == "1+D")
			s += "0011111";
		else if(comp == "A+1" || comp == "1+A")
			s += "0110111";
		else if(comp == "D-1")
			s += "0001110";
		else if(comp == "A-1")
			s += "0110010";
		else if(comp == "D+A" || comp == "A+D")
			s += "0000010";
		else if(comp == "D-A")
			s += "0010011";
		else if(comp == "A-D")
			s += "0000111";
		else if(comp == "D&A")
			s += "0000000";
		else if(comp == "D|A")
			s += "0010101";
		else if(comp == "M")
			s += "1110000";
		else if(comp == "!M")
			s += "1110001";
		else if(comp == "-M")
			s += "1110011";
		else if(comp == "M+1" || comp == "1+M")
			s += "1110111";
		else if(comp == "M-1")
			s += "1110010";
		else if(comp == "D+M" || comp == "M+D")
			s += "1000010";
		else if(comp == "D-M")
			s += "1010011";
		else if(comp == "M-D")
			s += "1000111";
		else if(comp == "D&M")
			s += "1000000";
		else if(comp == "D|M")
			s += "1010101";
		
		//destination generator
		if(dest == "")
			s += "000";
		else if(dest == "M")
			s += "001";
		else if(dest == "D")
			s += "010";
		else if(dest == "MD" || dest == "DM")
			s += "011";
		else if(dest == "A")
			s += "100";
		else if(dest == "AM" || dest == "MA")
			s += "101";
		else if(dest == "AD" || dest == "DA")
			s += "110";
		else if(dest == "AMD" || dest == "ADM" || dest == "DMA" || dest == "DAM" || dest == "MAD" || dest == "MDA")
			s += "111";
		
		//jmp generator
		if(jmp == "")
			s += "000";
		else if(jmp == "JGT")
			s += "001";
		else if(jmp == "JEQ")
			s += "010";
		else if(jmp == "JGE")
			s += "011";
		else if(jmp == "JLT")
			s += "100";
		else if(jmp == "JNE")
			s += "101";
		else if(jmp == "JLE")
			s += "110";
		else if(jmp == "JMP")
			s += "111";
	}
	return s;
}

int main()
{
	string filename, in, out;
	cout << "Enter the name of input file (without extension): ";
	cin >> filename;
	
	in = filename + ".asm";
	out = filename + ".hack";
	ifstream ifile(in);
	ofstream ofile(out);
	
	if(!ifile)
	{
		cout << "File not found.";
		exit(0);
	}
	
	cout << "Processing file..." << endl;
	
	while(!ifile.eof())
	{
		string line = readLine(ifile);
		if(line != "")
		{
			string bin = binGenerator(line);
			ofile << bin << endl;
		}
	}
	
	cout << "Conversion completed." << endl;
	ifile.close();
	ofile.close();
	return 0;
}