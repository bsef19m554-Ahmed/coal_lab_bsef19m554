SECTION .data
	num1	dd	0x41414141
	num2	dq	0x42424242
	EXIT_STATUS equ 0
	
SECTION .bss
	;nothing here

SECTION .text
	global _start
_start:
	mov rax, num1
	add rax, num2
	
;exit gracefully
	mov rax, 60
	mov rdi, EXIT_STATUS
	syscall