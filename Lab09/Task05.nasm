SECTION .data
	msg1 db "What is your name? "
	msg2 db "Welcome, "
	EXIT_STATUS equ 0
	
SECTION .bss
	var: resb 16
	
SECTION .text
	global _start
_start:
	
	mov rax,1
    mov rdi,1 
    mov rsi,msg1
    mov rdx,26
    syscall
	
	call _getVar
	
	mov rax,1
    mov rdi,1 
    mov rsi,msg2
    mov rdx,26
    syscall
	
	mov rax,1
    mov rdi,1 
    mov rsi,[var]
    mov rdx,26
    syscall
	
_getVar:
	mov rax,0
	mov rdi,0
	mov rsi, [var]
	mov rdx, 16
	syscall
	ret
	
;EXIT Gracefully
	mov rax, 60
	mov rdi, EXIT_STATUS
	syscall