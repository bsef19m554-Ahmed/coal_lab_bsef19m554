SECTION .data
	num1	db	0x4
	num2	db	0x32
	EXIT_STATUS equ 0
	
SECTION .bss
	res:	resb	16
	;nothing here

SECTION .text
	global _start
_start:
	mov byte [res], num2
	add byte [res], num2
	add byte [res], num1
	sub byte [res], num2
	push [res]

;exit gracefully
	mov rax, 60
	mov rdi, EXIT_STATUS
	syscall