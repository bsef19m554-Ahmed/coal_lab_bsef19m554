1. Compile nasm program using -g -felf64 flags
2. Link object file using ld
3. open executable in gdb -tui mode
4. Put a break on _start
5. run the program
6. use si once
7. when the program is on JE instruction, change the value of rip
8. It can be done using set $rip = "address where we want to go"
9. address can be found by layout asm
10. after modifying rip register, countinue the program
11. Secret msg should print on screen