#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <algorithm>
#include <map>

using namespace std;

string readLine(ifstream &file)
{
	string s;
	file >> ws;
	getline(file, s);
	
	s = regex_replace(s, regex(" "), "");
	s = regex_replace(s, regex("\t"), "");
	
	for(int i=0; i<s.length(); i++)
	{
		if(s[i] == '/')
		{
			int j = i;
			while(s[j+1] == ' ')
			{
				j++;
			}
			if(s[j+1] == '/')
			{
				s.resize(i);
				break;
			}
		}
	}
	
	return s;
}

string binGenerator(const string &str)
{
	string s;
	if(str[0] == '@')
	{
		int num = stoi(str.substr(1, str.length()));
		for(int i=0; i<16; i++)
		{
			s.insert(0, to_string(num%2));
			num = num/2;
		}
		return s;
	}
	else
	{
		s = "111";
		string comp, dest, jmp;
		int equalIndex = str.find("=");
		int jumpIndex = str.find(";");
		if(equalIndex == -1 && jumpIndex >= 0)
		{
			comp = str.substr(0, jumpIndex);
			dest = "";
			jmp = str.substr(jumpIndex+1, str.length()-1);
		}
		else if(equalIndex >= 0 && jumpIndex == -1)
		{
			dest = str.substr(0, equalIndex);
			comp = str.substr(equalIndex+1, str.length()-1);
			jmp = "";
		}
		else
		{
			throw string("Error, not a valid asm code.");
		}
		
		
		//computation generator
		if(comp == "0")
			s += "0101010";
		else if(comp == "1")
			s += "0111111";
		else if(comp == "-1")
			s += "0111010";
		else if(comp == "D")
			s += "0001100";
		else if(comp == "A")
			s += "0110000";
		else if(comp == "!D")
			s += "0001101";
		else if(comp == "!A")
			s += "0110001";
		else if(comp == "-D")
			s += "0001111";
		else if(comp == "-A")
			s += "0110011";
		else if(comp == "D+1" || comp == "1+D")
			s += "0011111";
		else if(comp == "A+1" || comp == "1+A")
			s += "0110111";
		else if(comp == "D-1")
			s += "0001110";
		else if(comp == "A-1")
			s += "0110010";
		else if(comp == "D+A" || comp == "A+D")
			s += "0000010";
		else if(comp == "D-A")
			s += "0010011";
		else if(comp == "A-D")
			s += "0000111";
		else if(comp == "D&A")
			s += "0000000";
		else if(comp == "D|A")
			s += "0010101";
		else if(comp == "M")
			s += "1110000";
		else if(comp == "!M")
			s += "1110001";
		else if(comp == "-M")
			s += "1110011";
		else if(comp == "M+1" || comp == "1+M")
			s += "1110111";
		else if(comp == "M-1")
			s += "1110010";
		else if(comp == "D+M" || comp == "M+D")
			s += "1000010";
		else if(comp == "D-M")
			s += "1010011";
		else if(comp == "M-D")
			s += "1000111";
		else if(comp == "D&M")
			s += "1000000";
		else if(comp == "D|M")
			s += "1010101";
		
		//destination generator
		if(dest == "")
			s += "000";
		else if(dest == "M")
			s += "001";
		else if(dest == "D")
			s += "010";
		else if(dest == "MD" || dest == "DM")
			s += "011";
		else if(dest == "A")
			s += "100";
		else if(dest == "AM" || dest == "MA")
			s += "101";
		else if(dest == "AD" || dest == "DA")
			s += "110";
		else if(dest == "AMD" || dest == "ADM" || dest == "DMA" || dest == "DAM" || dest == "MAD" || dest == "MDA")
			s += "111";
		
		//jmp generator
		if(jmp == "")
			s += "000";
		else if(jmp == "JGT")
			s += "001";
		else if(jmp == "JEQ")
			s += "010";
		else if(jmp == "JGE")
			s += "011";
		else if(jmp == "JLT")
			s += "100";
		else if(jmp == "JNE")
			s += "101";
		else if(jmp == "JLE")
			s += "110";
		else if(jmp == "JMP")
			s += "111";
	}
	return s;
}

string preDef(const string &str)
{
	string s = str;
	s = regex_replace(s, regex("@R0"), "@0");
	s = regex_replace(s, regex("@R1"), "@1");
	s = regex_replace(s, regex("@R2"), "@2");
	s = regex_replace(s, regex("@R3"), "@3");
	s = regex_replace(s, regex("@R4"), "@4");
	s = regex_replace(s, regex("@R5"), "@5");
	s = regex_replace(s, regex("@R6"), "@6");
	s = regex_replace(s, regex("@R7"), "@7");
	s = regex_replace(s, regex("@R8"), "@8");
	s = regex_replace(s, regex("@R9"), "@9");
	s = regex_replace(s, regex("@R10"), "@10");
	s = regex_replace(s, regex("@R11"), "@11");
	s = regex_replace(s, regex("@R12"), "@12");
	s = regex_replace(s, regex("@R13"), "@13");
	s = regex_replace(s, regex("@R14"), "@14");
	s = regex_replace(s, regex("@R15"), "@15");
	s = regex_replace(s, regex("@SCREEN"), "@16384");
	s = regex_replace(s, regex("@KBD"), "@24576");
	s = regex_replace(s, regex("@SP"), "@0");
	s = regex_replace(s, regex("@LCl"), "@1");
	s = regex_replace(s, regex("@ARG"), "@2");
	s = regex_replace(s, regex("@THIS"), "@3");
	s = regex_replace(s, regex("@THAT"), "@4");
	return s;
}

void process(string x, ofstream &file)
{
	// 1st pass
	string originalString = x;
	map<string, int> symbolTable;
	int labelCount = 0;
	int lineCount = count(x.begin(), x.end(), '\n');
	
	for (int i = 0; i < lineCount; i++)
	{
		string line = x.substr(0, x.find("\n"));
		x = x.substr(x.find("\n") + 1, x.length() - x.find("\n"));
		
		string label;
		if (line[0] == '(')
		{
			label = line.substr(1, line.find(")") - 1);
			symbolTable.insert(pair<string, int>(label, i-labelCount++));
		}
		
	}
	
	// 2nd pass
	x = originalString;
	int varCount = 16;
	for(int i = 0; i < lineCount; i++)
	{
		string line = x.substr(0, x.find("\n"));
		x = x.substr(x.find("\n") + 1, x.length() - x.find("\n"));
		
		if (line[0] == '@' && (line[1] <'0' || line[1] > '9'))
		{
			string tmp = line.substr(1, line.find("\n")-1);
			map<string, int>::iterator itr;
			itr = symbolTable.find(tmp);
			
			if (itr != symbolTable.end())
				file << binGenerator("@" + to_string(itr->second)) << endl;
			else
			{
				itr = symbolTable.find(tmp);
				
				if (itr != symbolTable.end())
					file << binGenerator("@" + to_string(itr->second)) << endl;
				else
				{
					symbolTable.insert(pair<string, int>(tmp, varCount));
					file << binGenerator("@" + to_string(varCount++)) << endl;
				}
			}
		}
		else if(line[0] != '(')
			file << binGenerator(line) << endl;
	}
}


int main()
{
	string filename, in, out;
	cout << "Enter the name of input file (without extension): ";
	cin >> filename;
	
	in = filename + ".asm";
	out = filename + ".hack";
	ifstream ifile(in);
	ofstream ofile(out);
	
	if(!ifile)
	{
		cout << "File not found.";
		exit(0);
	}
	
	cout << "Processing file..." << endl;
	
	string str;
	while(!ifile.eof())
	{
		string line = readLine(ifile);
		if(line != "")
		{
			str = str + line + '\n';
		}
	}
	
	str = preDef(str);
	process(str, ofile);
	
	cout << "Conversion completed." << endl;
	ifile.close();
	ofile.close();
	return 0;
}